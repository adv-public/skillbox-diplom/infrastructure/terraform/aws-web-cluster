# Web Server denav.net

This folder contains a configuration that deploys a cluster of web servers 
(using [EC2](https://aws.amazon.com/ec2/) and [Auto Scaling](https://aws.amazon.com/autoscaling/)) and a load balancer
(using [NLB](https://aws.amazon.com/elasticloadbalancing/)) in an [Amazon Web Services (AWS) 
account](http://aws.amazon.com/). The load balancer listens on port 80. 
Provides a Hetzner DNS Records resource to create, update and delete DNS Records[Terraform Registory](https://registry.terraform.io/providers/timohirt/hetznerdns/2.2.0).

## Pre-requisites

* You must have [Terraform](https://www.terraform.io/) installed on your computer.
* You must have [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) installed on your computer.
* You must have an [Amazon Web Services (AWS) account](https://aws.amazon.com/).
* You must have an [Hetzner account](https://accounts.hetzner.com/)

Please note that this code was written for Terraform 1.x.

## Quick start

**Please note that this example will deploy real resources into your AWS account. We have made every effort to ensure 
all the resources qualify for the [AWS Free Tier](https://aws.amazon.com/free/), but we are not responsible for any
charges you may incur.** 

Configure your [AWS access keys](http://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html#access-keys-and-secret-access-keys) as 
environment variables:

```
export AWS_ACCESS_KEY_ID=(your access key id)
export AWS_SECRET_ACCESS_KEY=(your secret access key)
```

Configure your [Hetzner API token](https://docs.hetzner.com/dns-console/dns/general/api-access-token)
Inject the API Token via the Environment

Assign the API token to `HETZNER_DNS_API_TOKEN` env variable.

```
export HETZNER_DNS_API_TOKEN=<your api token>
```

Deploy the code with Terraform:

```
cd terraform/stage/services
terraform init
terraform apply
```


When the `apply` command completes, it will output the DNS name of the load balancer. To test the load balancer:

```
curl http://<nlb_dns_name>/
or 
curl http://test.denav.net/
```

Clean up when you're done:

```
terraform destroy
```