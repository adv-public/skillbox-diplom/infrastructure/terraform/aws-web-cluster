terraform {
  required_version = ">= 1.0.0, < 2.0.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

## Configure the AWS Provider
# provider "aws" {
#   region = "eu-central-1"
# }

# Find out what data centers are in the selected region
# data "aws_availability_zones" "available" {}

## We are looking for an image with the latest version of Ubuntu
data "aws_ami" "ubuntu" {
  owners      = ["amazon"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }
}

module "webdenav_lc" {
  source = "../../../../../modules/terraform-aws-autoscaling"

  # labels
  name        = local.name
  environment = local.environment
  label_order = ["name", "environment"]

  # Launch configuration
  #
  # launch_configuration = "my-existing-launch-configuration" # Use the existing launch configuration
  # create_lc = false # disables creation of launch configuration
  lc_name = "lc-${local.name_prefix}"

  image_id        = data.aws_ami.ubuntu.id
  instance_type   = "t2.micro"
  security_groups = [ aws_security_group.instance.id ]
  key_name        = "dd-web_denav"

  associate_public_ip_address   = true
  recreate_asg_when_lc_changes  = true
  user_data                     = templatefile("user_data.sh", { })

  # Auto scaling group
  asg_name            = "asg-${local.name_prefix}"
  vpc_zone_identifier = tolist(local.subnet_id)
  health_check_type   = "EC2"
  # load_balancers = [aws_lb_target_group.nlb-tg.arn]
  target_group_arns   = [aws_lb_target_group.nlb-tg.arn]

  min_size = 2
  max_size = 4

  desired_capacity          = 2
  wait_for_capacity_timeout = 0

}

# Get instance IPs for Ansible
data "aws_instances" "instance_ips" {

  instance_state_names = ["pending", "running"]
  filter {
    name   = "tag:Name"
    values = [local.name_prefix]
  }
}
