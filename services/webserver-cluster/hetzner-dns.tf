# DNS-Record config
module "hetzner-dns" {
  source    = "git@gitlab.com:adv-public/skillbox-diplom/infrastructure/terraform/modules.git//terraform-hetzner-dns?ref=v0.0.1"

  dns_zone  = "denav.net"
  dns_name  = "test-${local.environment}"
  dns_value = data.terraform_remote_state.vpc-stage.outputs.eip-stage.public_ip
  dns_type  = "A"
  dns_ttl   = "3600"
}