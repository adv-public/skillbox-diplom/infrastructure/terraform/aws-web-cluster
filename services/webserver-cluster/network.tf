## Network
data "terraform_remote_state" "vpc-stage" {
  backend = "local"
  
  config = {
    path = "${path.module}/../../../../global/aws-vpc/state/terraform.tfstate"
     
   }
}

#Module      : Label
#Description : This terraform module is designed to generate consistent label names and
#              tags for resources. You can use terraform-labels to implement a strict
#              naming convention.
module "labels" {
  source  = "../../../../../modules/terraform-aws-labels"

  name        = local.name
  environment = local.environment
  managedby   = "denav"
  label_order = ["name", "environment"]
}


## elastic ip
# resource "aws_eip" "ip-nlb" {
#   tags = merge(
#     module.labels.tags,
#     {
#       Name = format("%s%s%s", module.labels.id, var.delimiter, "nlb-eip")
#     },
#   )
# }

## Network Load balancer resources
resource "aws_lb" "nlb_webdenav" {
  name               = "${local.name_prefix}-${var.nlb_name}"
  load_balancer_type = "network"
  internal           = false

  subnet_mapping {
    subnet_id     = local.subnet_id[0]
    # allocation_id = aws_eip.ip-nlb.id
    allocation_id = data.terraform_remote_state.vpc-stage.outputs.eip-stage.id
  }

  enable_deletion_protection = false
  enable_cross_zone_load_balancing = false # true
  
  tags = merge(
    module.labels.tags,
    {
      Name = format("%s%s%s", module.labels.id, var.delimiter, "nlb")
    }, var.tags
  )

}

# Create NLB target group that forwards traffic to alb
resource "aws_lb_target_group" "nlb-tg" {
  name        = "${local.name_prefix}-tg-nlb"
  vpc_id      = local.vpc_id
  port        = var.public_in_port
  protocol    = "TCP"
  target_type = "instance"

  # health_check {
  #   enabled  = true
	#   interval = 30
	#   port     = var.public_in_port
  # }

  tags = merge(
    module.labels.tags,
    {
      Name = format("%s%s%s", module.labels.id, var.delimiter, "nlb-tg")
    }, var.tags
  )
  
}

# nlb listener
resource "aws_lb_listener" "nlb-http" {
  load_balancer_arn = aws_lb.nlb_webdenav.arn
  port              = var.public_in_port
  protocol          = "TCP"

  # By default, return a simple 404 page
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.nlb-tg.arn
  }
}

# security group public
resource "aws_security_group" "public" {
  name = "${local.name_prefix}-${var.public_security_group_name}"

  description = "Allow Public Access to NLB"
  vpc_id      = local.vpc_id

  # Let's set a rule on which ports we can access our servers
  ingress = [ {
      description      = "Allow inbound HTTP requests"
      from_port        = var.public_in_port
      to_port          = var.public_in_port
      protocol         = "tcp"
      cidr_blocks      = [ "0.0.0.0/0" ]
      self             = false
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
  } ]

  egress = [ {
    cidr_blocks      = [ "0.0.0.0/0" ]
    description      = "Allow all outbound requests"
    from_port        = 0
    protocol         = "-1"
    self             = false
    to_port          = 0
    ipv6_cidr_blocks = []
    prefix_list_ids  = []
    security_groups  = []
  } ]

  tags = merge(
    module.labels.tags,
    {
      Name = format("%s%s%s", module.labels.id, var.delimiter, "allow-public")
    }, var.tags
  )
}

# security group instance
resource "aws_security_group" "instance" {
  name = "${local.name_prefix}-${var.instance_security_group_name}"

  description = "Allow Public Access to instance"
  vpc_id      = local.vpc_id

  # Let's set a rule on which ports we can access our servers
  egress = [ 
    {
      cidr_blocks      = [ "0.0.0.0/0" ]
      description      = "Allow all outbound requests"
      from_port        = 0
      protocol         = "-1"
      self             = false
      to_port          = 0
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
    } 
  ]

  tags = merge(
    module.labels.tags,
    {
      Name = format("%s%s%s", module.labels.id, var.delimiter, "allow-public")
    }, var.tags
  ) 

}

# security group rules
resource "aws_security_group_rule" "instance_in_http" {
  description      = "Allow inbound HTTP requests"
  type              = "ingress"
  from_port         = var.public_in_port
  to_port           = var.public_in_port
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.instance.id
}

resource "aws_security_group_rule" "instance_in_ssh" {
  description      = "Allow inbound SSH requests"
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.instance.id
}
