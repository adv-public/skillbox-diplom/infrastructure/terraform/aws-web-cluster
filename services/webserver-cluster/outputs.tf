output "nlb_dns_name" {
  value       = aws_lb.nlb_webdenav.dns_name
  description = "The domain name of the load balancer"
}

output "nlb_eip" {
  description = "Contains the public IP address for NLB"
  value       = data.terraform_remote_state.vpc-stage.outputs.eip-stage.public_ip
}

output "instance_ips" {
  value = data.aws_instances.instance_ips.public_ips
  description = "IP address from instance"
}
